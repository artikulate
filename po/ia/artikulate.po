# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the artikulate package.
#
# Giovanni Sora <g.sora@tiscali.it>, 2020, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: artikulate\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:22+0000\n"
"PO-Revision-Date: 2023-07-06 11:57+0200\n"
"Last-Translator: giovanni <g.sora@tiscali.it>\n"
"Language-Team: Interlingua <kde-i18n-doc@kde.org>\n"
"Language: ia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#: data/languages/ba.xml:9
#, kde-format
msgctxt "Language Title"
msgid "Bengali"
msgstr "Bengalese"

#: data/languages/ca.xml:9
#, kde-format
msgctxt "Language Title"
msgid "Catalan"
msgstr "Catalan "

#: data/languages/de.xml:9
#, kde-format
msgctxt "Language Title"
msgid "German"
msgstr "Germano "

#: data/languages/du.xml:9
#, kde-format
msgctxt "Language Title"
msgid "Dutch"
msgstr "Hollandese "

#: data/languages/en_BE.xml:10
#, kde-format
msgctxt "Language Title"
msgid "British English"
msgstr "Anglese Britannic "

#: data/languages/en_US.xml:9
#, kde-format
msgctxt "Language Title"
msgid "English"
msgstr "Anglese"

#: data/languages/fr.xml:9
#, kde-format
msgctxt "Language Title"
msgid "French"
msgstr "Francese"

#: data/languages/gr.xml:9
#, kde-format
msgctxt "Language Title"
msgid "Greek"
msgstr "Greco"

#: data/languages/hi.xml:9
#, kde-format
msgctxt "Language Title"
msgid "Hindi"
msgstr "Hindi "

#: data/languages/it.xml:9
#, kde-format
msgctxt "Language Title"
msgid "Italian"
msgstr "Italiano "

#: data/languages/mr.xml:10
#, kde-format
msgctxt "Language Title"
msgid "Marathi"
msgstr "Marathi"

#: data/languages/pl.xml:9
#, kde-format
msgctxt "Language Title"
msgid "Polish"
msgstr "Polonese "

#: data/languages/sp.xml:9
#, kde-format
msgctxt "Language Title"
msgid "Spanish"
msgstr "Espaniol "

#: liblearnerprofile/src/models/learninggoalmodel.cpp:154
#, kde-format
msgctxt "@item:inlistbox unknown learning goal"
msgid "unknown"
msgstr "incognite"

#: liblearnerprofile/src/models/learninggoalmodel.cpp:219
#, kde-format
msgctxt "@title:column"
msgid "Learning Goal"
msgstr "Scopo de apprender"

#: liblearnerprofile/src/profilemanager.cpp:146
#, kde-format
msgid "Open Image"
msgstr "Aperi imagine"

#: liblearnerprofile/src/profilemanager.cpp:146
#, kde-format
msgid "Image Files (*.png *.jpg *.bmp)"
msgstr "Files de Image  (*png *.jpg *.bmp)"

#: liblearnerprofile/src/storage.cpp:567
#, kde-format
msgid "Invalid database version '%1'."
msgstr "Version invalide de base de datos '%1'."

#. i18n: ectx: label, entry (ShowMenuBar), group (artikulate)
#: src/artikulate.kcfg:13
#, kde-format
msgid "If true, the main menu bar is shown."
msgstr "Si true (ver), le barra de menu principal es monstrate."

#. i18n: ectx: label, entry (UseCourseRepository), group (artikulate)
#: src/artikulate.kcfg:17
#, kde-format
msgid ""
"If enabled, course files are only read from the local contributor repository"
msgstr ""
"Si activate, diles de curso es solmente legite ex le repositorio de "
"contributor local"

#. i18n: ectx: label, entry (CourseRepositoryPath), group (artikulate)
#: src/artikulate.kcfg:21
#, kde-format
msgid "Path to local contributor repository"
msgstr "Percurso al repositorio del contributor local"

#. i18n: ectx: label, entry (AudioInputDevice), group (artikulate)
#: src/artikulate.kcfg:24
#, kde-format
msgid "Name of audio input device"
msgstr "Nomine de dispositivo de ingresso audio"

#. i18n: ectx: label, entry (AudioInputVolume), group (artikulate)
#: src/artikulate.kcfg:27
#, kde-format
msgid "Audio input volume"
msgstr "Volumine de ingresso audio"

#. i18n: ectx: label, entry (AudioOutputVolume), group (artikulate)
#: src/artikulate.kcfg:31
#, kde-format
msgid "Audio output volume"
msgstr "Volumine de exito audio"

#. i18n: ectx: label, entry (TrainingPhraseFont), group (artikulate)
#: src/artikulate.kcfg:35
#, kde-format
msgid "Font"
msgstr "Font"

#. i18n: ectx: label, entry (RecordingBackend), group (artikulate)
#: src/artikulate.kcfg:42
#, kde-format
msgid ""
"Set either to 'qtmultimediabackend' or 'qtgstreamerbackend', selected on "
"runtime if available."
msgstr ""
"Fixar o a 'qtmultimediabackend' o 'qtgstreamerbackend', seligite sur tempore "
"de execution (runtime) si disponibile."

#: src/core/drawertrainingactions.cpp:15
#, kde-format
msgid "Please select a course"
msgstr "Pro favor tu selige un curso"

#: src/core/resources/editablecourseresource.cpp:262
#, kde-format
msgid "New Unit"
msgstr "Nove Unitate"

#: src/main.cpp:27
#, kde-format
msgctxt "@title Displayed program name"
msgid "Artikulate"
msgstr "Artikulate"

#: src/main.cpp:29
#, kde-format
msgctxt "@title KAboutData: short program description"
msgid "Artikulate Pronunciation Trainer"
msgstr "Instructor de pronunciation Artikulate"

#: src/main.cpp:31 src/main_editor.cpp:30
#, kde-format
msgctxt "@info:credit"
msgid "(c) 2013-2022 The Artikulate Developers"
msgstr "(c) 2013-2022 Le Disveloppatores de Artikulate"

#: src/main.cpp:32
#, kde-format
msgctxt "@title Short program description"
msgid "Train your pronunciation in a foreign language."
msgstr "Traina tu pronunciation in un linguage estranie."

#: src/main.cpp:35 src/main_editor.cpp:33
#, kde-format
msgctxt "@info:credit Developer name"
msgid "Andreas Cord-Landwehr"
msgstr "Andreas Cord-Landwehr"

#: src/main.cpp:36 src/main_editor.cpp:34
#, kde-format
msgctxt "@info:credit Role"
msgid "Original Author"
msgstr "Autor original"

#: src/main.cpp:39
#, kde-format
msgctxt "@info:credit Developer name"
msgid "Samikshan Bairagya"
msgstr "Samikshan Bairagya"

#: src/main.cpp:40
#, kde-format
msgctxt "@info:credit Role"
msgid "Developer"
msgstr "Disveloppator"

#: src/main.cpp:43
#, kde-format
msgctxt "@info:credit Developer name"
msgid "Oindrila Gupta"
msgstr "Oindrila Gupta"

#: src/main.cpp:44 src/main.cpp:48
#, kde-format
msgctxt "@info:credit Role"
msgid "Developer and Course Data"
msgstr "Datos de Disveloppator e de Curso"

#: src/main.cpp:47
#, kde-format
msgctxt "@info:credit Developer name"
msgid "Magdalena Konkiewicz"
msgstr "Magdalena Konkiewicz"

#: src/main.cpp:51
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Giovanni Sora"

#: src/main.cpp:51
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "g.sora@tiscali.it"

#: src/main_editor.cpp:26
#, kde-format
msgctxt "@title Displayed program name"
msgid "Artikulate Editor"
msgstr "Editor de Artikulate"

#: src/main_editor.cpp:28
#, kde-format
msgctxt "@title KAboutData: short program description"
msgid "Artikulate Course Editor"
msgstr "Editor de curso Artikulate"

#: src/main_editor.cpp:31
#, kde-format
msgctxt "@title Short program description"
msgid "Edit Artikulate course files."
msgstr "Modifica files de curso de Artikulate."

#: src/mainwindow.cpp:36 src/qml/ProfileSelector.qml:18
#, kde-format
msgid "Unnamed Identity"
msgstr "Identitate non nominate"

#: src/models/coursemodel.cpp:99 src/models/languageresourcemodel.cpp:71
#: src/models/phonememodel.cpp:61 src/models/phraselistmodel.cpp:96
#: src/models/profilemodel.cpp:87 src/models/skeletonmodel.cpp:96
#: src/models/unitmodel.cpp:84
#, kde-format
msgctxt "@item:inlistbox:"
msgid "unknown"
msgstr "incognite"

#: src/models/coursemodel.cpp:158
#, kde-format
msgctxt "@title:column"
msgid "Course"
msgstr "Curso"

#: src/models/languageresourcemodel.cpp:150
#, kde-format
msgctxt "@title:column"
msgid "Language"
msgstr "Linguage"

#: src/models/learningprogressmodel.cpp:132
#, kde-format
msgid "Words"
msgstr "Parolas"

#: src/models/learningprogressmodel.cpp:134
#, kde-format
msgid "Sentences"
msgstr "Phrases"

#: src/models/learningprogressmodel.cpp:136
#, kde-format
msgid "Expressions"
msgstr "Expressiones"

#: src/models/learningprogressmodel.cpp:138
#, kde-format
msgid "Paragraphs"
msgstr "Paragraphos"

#: src/models/phonemegroupmodel.cpp:153
#, kde-format
msgctxt "@title:column"
msgid "Phoneme Group"
msgstr "Grupo de Phonema"

#: src/models/phonememodel.cpp:123
#, kde-format
msgctxt "@title:column"
msgid "Phoneme"
msgstr "Phonema"

#: src/models/phonemeunitmodel.cpp:182
#, kde-format
msgctxt "@title:column"
msgid "Phoneme Unit"
msgstr "Unitate de Phonema"

#: src/models/phraselistmodel.cpp:172 src/models/phrasemodel.cpp:294
#, kde-format
msgctxt "@title:column"
msgid "Phrase"
msgstr "Phrase"

#: src/models/profilemodel.cpp:146
#, kde-format
msgctxt "@title:column"
msgid "Profile"
msgstr "Profilo"

#: src/models/skeletonmodel.cpp:155
#, kde-format
msgctxt "@title:column"
msgid "Skeleton"
msgstr "Skeleto"

#: src/models/unitmodel.cpp:155
#, kde-format
msgctxt "@title:column"
msgid "Unit"
msgstr "Unitate"

#: src/qml/ArtikulateDrawer.qml:35
#, kde-format
msgid "Training"
msgstr "Exercitation"

#: src/qml/ArtikulateDrawer.qml:128 src/qml/WelcomePage.qml:80
#, kde-format
msgid "Download Training"
msgstr "Discarga exercitation"

#: src/qml/ArtikulateDrawer.qml:140
#, kde-format
msgid "About Artikulate"
msgstr "A proposito de Artikulate"

#: src/qml/CourseConfigurationPage.qml:15 src/qml/EditorDrawer.qml:58
#, kde-format
msgid "Course Configuration"
msgstr "Configuration de curso"

#: src/qml/CourseConfigurationPage.qml:15
#, kde-format
msgid "Prototype Configuration"
msgstr "Configuration de Prototypo"

#: src/qml/CourseConfigurationPage.qml:20
#, kde-format
msgid "Prototype Description"
msgstr "Description de Prototypo"

#: src/qml/CourseConfigurationPage.qml:20
#, kde-format
msgid "Course Description"
msgstr "Description de curso"

#: src/qml/CourseConfigurationPage.qml:24
#, kde-format
msgid "Title:"
msgstr "Titulo:"

#: src/qml/CourseConfigurationPage.qml:31
#, kde-format
msgid "Localized Title:"
msgstr "Titulo localisate:"

#: src/qml/CourseConfigurationPage.qml:37
#, kde-format
msgid "Description:"
msgstr "Description:"

#: src/qml/CourseConfigurationPage.qml:44
#, kde-format
msgid "Language:"
msgstr "Linguage:"

#: src/qml/CourseConfigurationPage.qml:52
#, kde-format
msgid "Prototype"
msgstr "Prototypo"

#: src/qml/CourseConfigurationPage.qml:57
#, kde-format
msgid "Update from Prototype:"
msgstr "Actualisa ab Prototypo:"

#: src/qml/CourseConfigurationPage.qml:60
#, kde-format
msgid "Update"
msgstr "Actualisa"

#: src/qml/CourseConfigurationPage.qml:65
#, kde-format
msgid "Update the course with elements from prototype."
msgstr "Actualisa le curso con elementos ab prototypo."

#: src/qml/EditCoursePage.qml:16
#, kde-format
msgid "Edit Course"
msgstr "Modifica Curso"

#: src/qml/EditCoursePage.qml:25
#, kde-format
msgid "Previous"
msgstr "Precedente"

#: src/qml/EditCoursePage.qml:26
#, kde-format
msgid "Switch to previous phrase."
msgstr "Commuta a Previe phrase."

#: src/qml/EditCoursePage.qml:32 src/qml/TrainingPage.qml:40
#, kde-format
msgid "Next"
msgstr "Proxime"

#: src/qml/EditCoursePage.qml:33
#, kde-format
msgid "Switch to next phrase."
msgstr "Commuta a Proxime phrase."

#: src/qml/EditCoursePage.qml:40 src/qml/ProfileUserItem.qml:73
#: src/qml/ProfileUserItem.qml:170
#, kde-format
msgid "Delete"
msgstr "Dele"

#: src/qml/EditCoursePage.qml:41
#, kde-format
msgid "Delete this phrase."
msgstr "Dele iste phrase."

#: src/qml/EditCoursePage.qml:49
#, kde-format
msgid "Create Phrase"
msgstr "Crea Phrase"

#: src/qml/EditCoursePage.qml:50
#, kde-format
msgid "Create phrase after current phrase."
msgstr "Crea phrase post phrase currente"

#: src/qml/EditorCourseSelectionPage.qml:15
#, kde-format
msgid "Welcome to Artikulate Course Editor"
msgstr "Benvenite al Editor de Curso Artikulate"

#: src/qml/EditorCourseSelectionPage.qml:46 src/qml/WelcomePage.qml:48
#, kde-format
msgctxt "@title:window language / course name"
msgid "%1 / %2"
msgstr "%1 / %2"

#: src/qml/EditorCourseSelectionPage.qml:60
#, kde-format
msgctxt "@action:button"
msgid "Edit Course"
msgstr "Modifica Curso"

#: src/qml/EditorDrawer.qml:34
#, kde-format
msgid "Courses"
msgstr "Cursos"

#: src/qml/EditorDrawer.qml:45
#, kde-format
msgid "Repository"
msgstr "Repositorio:"

#: src/qml/EditorDrawer.qml:121
#, kde-format
msgid "Save"
msgstr "Salveguarda"

#: src/qml/EditorDrawer.qml:147
#, kde-format
msgid "About Artikulate Editor"
msgstr "A proposito del Editor de Artikulate"

#: src/qml/EditorSkeletonSelectionPage.qml:15
#, kde-format
msgid "Select Prototype"
msgstr "Selige Prototypo"

#: src/qml/EditorSkeletonSelectionPage.qml:46
#, kde-format
msgctxt "@title:window prototype name"
msgid "%1"
msgstr "%1"

#: src/qml/EditorSkeletonSelectionPage.qml:60
#, kde-format
msgctxt "@action:button"
msgid "Edit Prototype"
msgstr "Edita Prototypo"

#: src/qml/PhraseEditor.qml:106
#, kde-format
msgid "Unit"
msgstr "Unitate"

#: src/qml/PhraseEditor.qml:106
#, kde-format
msgid "Unit: "
msgstr "Unitate:"

#: src/qml/PhraseEditor.qml:111
#, kde-format
msgid "Localized Unit:"
msgstr "Unitate Localisate:"

#: src/qml/PhraseEditor.qml:122
#, kde-format
msgid "Phrase: "
msgstr "Phrase: "

#: src/qml/PhraseEditor.qml:127
#, kde-format
msgid "Localized Phrase:"
msgstr "Phrase Localisate:"

#: src/qml/PhraseEditorEditStateComponent.qml:48
#, kde-format
msgid "Edit State:"
msgstr "Edita Stato:"

#: src/qml/PhraseEditorEditStateComponent.qml:53
#, kde-format
msgctxt "state"
msgid "Unknown"
msgstr "Incognite"

#: src/qml/PhraseEditorEditStateComponent.qml:62
#, kde-format
msgctxt "state"
msgid "Translated"
msgstr "Traducite"

#: src/qml/PhraseEditorEditStateComponent.qml:71
#, kde-format
msgctxt "state"
msgid "Completed"
msgstr "Completate"

#: src/qml/PhraseEditorSoundComponent.qml:24
#, kde-format
msgid "Native Speaker Recording"
msgstr "Registration de parlator native"

#: src/qml/PhraseEditorSoundComponent.qml:34
#, kde-format
msgid "Existing Recording:"
msgstr "Registration existente:"

#: src/qml/PhraseEditorSoundComponent.qml:44
#, kde-format
msgid "Create New Recording:"
msgstr "Crea nove registration:"

#: src/qml/PhraseEditorSoundComponent.qml:60
#, kde-format
msgid "Replace Existing Recording"
msgstr "Reimplacia registration existente"

#: src/qml/PhraseEditorSoundComponent.qml:68
#, kde-format
msgid "Dismiss"
msgstr "Abbandona"

#: src/qml/PhraseEditorTypeComponent.qml:51
#, kde-format
msgid "Difficulty:"
msgstr "Difficultate:"

#: src/qml/PhraseEditorTypeComponent.qml:57
#, kde-format
msgid "Word"
msgstr "Parola"

#: src/qml/PhraseEditorTypeComponent.qml:65
#, kde-format
msgid "Expression"
msgstr "Expression"

#: src/qml/PhraseEditorTypeComponent.qml:73
#, kde-format
msgid "Sentence"
msgstr "Phrase"

#: src/qml/PhraseEditorTypeComponent.qml:81
#, kde-format
msgid "Paragraph"
msgstr "Paragrapho"

#: src/qml/ProfileDetailsItem.qml:38
#, kde-format
msgid "Learner"
msgstr "Studente"

#: src/qml/ProfileDetailsItem.qml:51
#, kde-format
msgid "Favorite Languages"
msgstr "Linguages favorite"

#: src/qml/ProfileSelector.qml:70
#, kde-format
msgid "Create New Learner Identity"
msgstr "Crea nove identitate de studente"

#: src/qml/ProfileSelector.qml:103
#, kde-format
msgid "Use Selected Identity"
msgstr "Usa identitate seligite"

#: src/qml/ProfileSettingsPage.qml:17
#, kde-format
msgid "Configure Profile"
msgstr "Configura profilo"

#: src/qml/ProfileSettingsPage.qml:29 src/qml/ProfileUserItem.qml:68
#, kde-format
msgid "Edit"
msgstr "Modifica"

#: src/qml/ProfileSettingsPage.qml:38
#, kde-format
msgid "Select Image"
msgstr "Selige Image"

#: src/qml/ProfileSettingsPage.qml:45 src/qml/ProfileUserItem.qml:84
#, kde-format
msgid "Clear Image"
msgstr "Netta image"

#: src/qml/ProfileSettingsPage.qml:51
#, kde-format
msgid "Delete User"
msgstr "Dele usator"

#: src/qml/ProfileSettingsPage.qml:101 src/qml/ProfileUserItem.qml:117
#, kde-format
msgid "Name"
msgstr "Nomine"

#: src/qml/ProfileUserItem.qml:79
#, kde-format
msgid "Change Image"
msgstr "Cambia  imagine"

#: src/qml/ProfileUserItem.qml:123
#, kde-format
msgid "Done"
msgstr "Facite"

#: src/qml/ProfileUserItem.qml:161
#, kde-format
msgid "Do you really want to delete this identity \"<b>%1</b>\"?"
msgstr "Tu vermente vole deler iste identitate \"<b>%1</b>\"?"

#: src/qml/ProfileUserItem.qml:177
#, kde-format
msgid "Cancel"
msgstr "Cancella"

#: src/qml/RepositoryConfigurationPage.qml:16
#, kde-format
msgid "Repository Configuration"
msgstr "Configuration de repositorio"

#: src/qml/RepositoryConfigurationPage.qml:21
#, kde-format
msgid "Select Repository Folder"
msgstr "Selige dossier de repositorio"

#: src/qml/RepositoryConfigurationPage.qml:34
#, kde-format
msgid "Repository:"
msgstr "Repositorio: "

#: src/qml/RepositoryConfigurationPage.qml:39
#, kde-format
msgid "Change Folder"
msgstr "Cambia Dossier"

#: src/qml/SoundPlayer.qml:19
#, kde-format
msgid "Play"
msgstr "Reproduce"

#: src/qml/SoundRecorder.qml:19
#, kde-format
msgid "Record"
msgstr "Registra"

#: src/qml/TrainerCourseStatistics.qml:46
#, kde-format
msgid "Attempts"
msgstr "Tentativas"

#: src/qml/TrainingPage.qml:28
#, kde-format
msgid "Category: no category selected"
msgstr "Categoria: nulle catecoria seligite"

#: src/qml/TrainingPage.qml:31
#, kde-format
msgid "Category: %1"
msgstr "Categoria: %1"

#: src/qml/TrainingPage.qml:41
#, kde-format
msgid "Mark current phrase as completed and proceed with next one."
msgstr "Marca phrase currente como completate e procede con le proxime."

#: src/qml/TrainingPage.qml:46
#, kde-format
msgid "Skip"
msgstr "Salta"

#: src/qml/TrainingPage.qml:47
#, kde-format
msgid "Skip current phrase and proceed with next one."
msgstr "Salta phrase currente e procede con le proxime."

#: src/qml/TrainingPage.qml:102
#, kde-format
msgid "Play original"
msgstr "Reproduce original"

#: src/qml/TrainingPage.qml:143
#, kde-format
msgid "Record yourself"
msgstr "Registra te mesme"

#: src/qml/TrainingPage.qml:151
#, kde-format
msgid "Play yourself"
msgstr "Reproduce te mesme"

#: src/qml/WelcomePage.qml:17
#, kde-format
msgid "Welcome to Artikulate"
msgstr "Benvenite a Artikulate"

#: src/qml/WelcomePage.qml:62
#, kde-format
msgctxt "@action:button"
msgid "Start Training"
msgstr "Initia exercitation"

#: src/qml/WelcomePage.qml:76
#, kde-format
msgid "No trainings found"
msgstr "On trovava nulle exercitation"

#~ msgid "Download Training Material"
#~ msgstr "Discarga material de exercitation"

#~ msgctxt "@action:button"
#~ msgid "Update"
#~ msgstr "Actualisa"

#~ msgctxt "@action:button"
#~ msgid "Install"
#~ msgstr "Installa"

#~ msgctxt "@action:button"
#~ msgid "Remove"
#~ msgstr "Remove"

#~ msgctxt "default sound device"
#~ msgid "Default"
#~ msgstr "Predefinite"

#~ msgctxt "@info:credit"
#~ msgid "(c) 2013-2019 The Artikulate Developers"
#~ msgstr "(c) 2013-2019 Le Disveloppatores de Artikulate"

#~ msgid "About"
#~ msgstr "A proposito"
